# EKS-Terraform

## Getting started

Current configurtion was tested using such versions:

- `HELM` Version: v3.15.2
- `Kubectl`: Client Version: v1.30.2; Kustomize Version: v5.0.4-0.20230601165947-6ce0bf390ce3
- `AWS CLI` Version: aws-cli/2.17.1 Python/3.11.9 Darwin/23.4.0 source/arm64
- `Terraform` Version: v1.5.2 on darwin_amd64

## Common setup:
- Install Terraform: https://developer.hashicorp.com/terraform/tutorials/aws-get-started/install-cli 
- Install kubectl and helm. Example for MacOS: (brew install kubectl && brew install helm)
- Configure AWS CLI: https://docs.aws.amazon.com/cli/v1/userguide/cli-chap-configure.html

### Configure kubectl to connect to EKS
```
aws eks --region eu-north-1 update-kubeconfig --name miabaza-cluster1
kubectl cluster-info
```
# How to use Terraform repo and run deploy:
- Clone repository
- Create AWS Access/Secret Keys
- Create `eks-terraform/aws-creds` file using `aws-creds.example` and add your AWS Access/Secret Keys to this file
- Go to the cloned `eks-terraform` folder and deploy:
```
terraform init
terraform plan
terraform apply
```
