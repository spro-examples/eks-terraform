################################################################################
# Bucket for state storing & required Providers
################################################################################

terraform {
  backend "s3" {
    shared_credentials_file = "aws-creds"
    bucket                  = "terraform.states.mia.corp"
    key                     = "mia-baza/eks_terraform_tfstate"
    region                  = "eu-north-1"
  }

  required_providers {
    aws = {
      source = "hashicorp/aws"
    }
    helm = {
      source  = "hashicorp/helm"
      version = "~> 2.0"
    }
    kubectl = {
      source  = "gavinbunney/kubectl"
      version = "~> 1.14"
    }
  }
}

################################################################################
# Providers 
################################################################################

provider "aws" {
  region = var.region
  alias  = "north"
  shared_credentials_files = ["aws-creds"]
}

# provider for aws public ECR
provider "aws" {
  region = "us-east-1"
  alias = "virginia"
}

provider "helm" {
  kubernetes {
    host                   = module.eks.cluster_endpoint
    cluster_ca_certificate = base64decode(module.eks.cluster_certificate_authority_data)

    exec {
      api_version = "client.authentication.k8s.io/v1beta1"
      command     = "aws"
      # This requires the awscli to be installed locally where Terraform is executed
      args = ["eks", "get-token", "--cluster-name", module.eks.cluster_name]
    }
  }
}

provider "kubectl" {
  host                   = module.eks.cluster_endpoint
  cluster_ca_certificate = base64decode(module.eks.cluster_certificate_authority_data)
  token                  = data.aws_eks_cluster_auth.cluster.token
}
